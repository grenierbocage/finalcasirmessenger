# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.conf.urls import patterns, url

urlpatterns = patterns('promotion.views',
                       url('^$', 'index', name="index"))
