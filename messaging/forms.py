
from __future__ import unicode_literals

from django import forms
from messaging.models import Message
from members.models import Member


class newMessageForm(forms.ModelForm):
    class Meta:
        model = Message
        exclude = []
        
    def __init__(self, member, *args, **kwargs):
        super(newMessageForm, self).__init__(*args, **kwargs)
        self.fields["recipient"].queryset = member.friends.all()
        self.fields["author"].widget = forms.HiddenInput()
        self.fields['is_read'].widget = forms.HiddenInput()

        
"""class newMessageForm(forms.Form):
    friends = forms.ModelMultipleChoiceField(queryset=None, widget=forms.CheckboxSelectMultiple)
    message = forms.CharField(label="Votre message",
                               max_length=254,
                               widget=forms.TextInput(attrs={"class": "form-control",
                                                             "placeholder": "Votre message"})) 
    dureeAffichage = forms.ModelMultipleChoiceField(queryset=None, widget=forms.CheckboxSelectMultiple) 
    file = forms.FileField()
    
    def __init__(self, member, *args, **kwargs):
        super(newMessageForm, self).__init__(*args, **kwargs)
        friends_list = member.friends.all()

        self.fields["friends"].queryset = friends_list"""
