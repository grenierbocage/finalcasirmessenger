# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from members.models import Member
from messaging.models import Message
from members.forms import AddFriendForm
from members.forms import PendingRequestForm
from messaging.forms import newMessageForm

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render

import json
from django.http import JsonResponse
@login_required
def index(request, friend_pk = None):
    current_member = request.user.member
    if request.method == "POST":
        if "add_friend_request" in request.POST:
            add_friend_form = AddFriendForm(member=current_member, data=request.POST)
            pending_request_form = PendingRequestForm(member=current_member)
            if add_friend_form.is_valid():
                for member in add_friend_form.cleaned_data["members"]:
                    member.friend_requests.add(current_member)
                add_friend_form = AddFriendForm(member=current_member)

                messages.success(request, "Demandes d'ajout effectuées avec succès")
        else:
            pending_request_form = PendingRequestForm(member=current_member, data=request.POST)
            add_friend_form = AddFriendForm(member=current_member)
            if pending_request_form.is_valid():
                if "refuse_pending_request" in request.POST:
                    for member in pending_request_form.cleaned_data["members"]:
                        current_member.friend_requests.remove(member)
                        messages.success(request, "Demandes en attente refusées avec succès")
                elif "accept_pending_request" in request.POST:
                    for member in pending_request_form.cleaned_data["members"]:
                        current_member.friend_requests.remove(member)
                        current_member.friends.add(member)
                    messages.success(request, "Demandes en attente acceptées avec succès")
            else:
                messages.warning(request, "Demandes en attente : opération annulée")
    elif request.method == "GET":
        add_friend_form = AddFriendForm(member=current_member)
        pending_request_form = PendingRequestForm(member=current_member)

    friends = current_member.friends.all()
    friend_requests = current_member.friend_requests.all()
    
    if friend_pk is not None :
        friend_en_cours = Member.objects.get(pk=friend_pk)
        liste_message = Message.objects.all().filter(recipient=current_member).filter(author=friend_en_cours).order_by("-created_on")
    else :
        friend_en_cours = None
        liste_message = Message.objects.all().filter(recipient = current_member).order_by("-created_on")
    page = request.GET.get('page')
    paginator = Paginator(liste_message, 5) # Show 5 messages per page

    try:
        messagesList = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        messagesList = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        messagesList = paginator.page(paginator.num_pages)

#     if(trie) :
#         liste_message = Message.objects.all().filter(recipient = current_member).filter(author= friend.pk)
        
    context = {'friends': friends,
               'friend_requests': friend_requests,
               'add_friend_form': add_friend_form,
               'liste_message':liste_message,
               'pending_request_form': pending_request_form,
               'messagesList' : messagesList}

    return render(request, "dashboard/index.html", context)


@login_required
def remove_friend(request, friend_pk):
    current_member = request.user.member
    removed_friend = get_object_or_404(Member, pk=friend_pk)
    current_member.friends.remove(removed_friend)
    messages.success(request, "Suppression effectuée avec succès")
    return redirect("dashboard:index")

def new_message(request):
    current_member = request.user.member
    if request.method == "GET":
        new_message_form = newMessageForm(member=current_member, initial={"author": current_member})
    elif request.method == "POST":

        new_message_form = newMessageForm(member=current_member, initial={"author": current_member}, data=request.POST, files=request.FILES)
        if "new_message" in request.POST :
            #print(new_message_form.errors)
            if new_message_form.is_valid():
                new_message = Message( author=new_message_form.cleaned_data["author"],
                                   recipient = new_message_form.cleaned_data["recipient"], 
                                   is_read=False, 
                                   content=new_message_form.cleaned_data["content"],
                                   duration = new_message_form.cleaned_data["duration"], 
                                   picture = new_message_form.cleaned_data["picture"])
                new_message.save()
                messages.success(request, "Message envoyé avec succès")
                
    
    context = {'new_message_form' : new_message_form}
    
    return render(request, "dashboard/message.html", context)

def chargerMessage(request):
    
    idMsg = request.POST.get("id")
    msg = Message.objects.get(id=idMsg)
    msg.is_read = True
    msg.save()
    data = {'msg':msg.content, 'duree':msg.duration, 'pathPhoto' : msg.picture.url if msg.picture else None}
    return JsonResponse(data)

